# frozen_string_literal: true

require 'json'

module Resources
  class Campaign
    class << self
      def create(args)
        result = DBService.insert('campaigns', args)
        result
      end

      def get_all
        remote_campaigns = AdService.get_remote_ads
        remote = JSON.parse(remote_campaigns)
        local_campaigns = DBService.select('campaigns')
        local = local_campaigns.map { |campaign| to_struct(campaign) }
        normalize_campaigns(remote, local)
      end

      def to_struct(db_campaign)
        c = {}
        c[:id], c[:job_id], c[:status], c[:external_reference], c[:ad_description] = db_campaign
        c
      end

      def normalize_campaigns(remote_campaigns, local_campaigns)
        result = []
        local_campaigns.each do |local|
          remote = remote_campaigns['ads'].select { |remote| remote['reference'].to_i == local[:external_reference] }
          next unless remote.length > 0

          discrepancies = []
          remote_camp = remote.first
          if remote_camp['status'] != local[:status]
            obj = { status: { remote: remote_camp['status'], local: local[:status] } }
            discrepancies << obj
          end
          if remote_camp['description'] != local[:ad_description]
            obj = { description: { remote: remote_camp['description'], local: local[:ad_description] } }
            discrepancies << obj
          end
          result << { remote_reference: remote_camp['reference'], discrepancies: discrepancies }
        end
        result
      end
    end
  end
end
