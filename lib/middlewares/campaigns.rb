# frozen_string_literal: true

module Rack
  class Campaign
    def initialize(app)
      @app = app
    end

    def call(env)
      request = Request.new(env)
      request_path = request.env['PATH_INFO']

      return CampaignService.process_request(request) if /\bcampaigns\b/ =~ request_path

      [200, { 'Content-Type' => 'text/html' }, []]
    end
  end
end
