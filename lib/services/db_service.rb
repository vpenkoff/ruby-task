# frozen_string_literal: true

module DBService
  class << self
    def open_db
      @db ||= SQLite3::Database.new 'test.db'
    end

    def insert(table, data)
      open_db
      @db.execute("insert into #{table} (#{data.keys.join(',')})
                  values (#{data.keys.map { |_key| '?' }.join(',')})", data.values)
    end

    def select(table, *args)
      open_db
      conditions = args.each_slice(2).to_a.map { |slice| slice.join('=') }.join(' AND ')
      if conditions != ''
        return @db.execute("select * from #{table} where #{conditions}")
      else
        return @db.execute("select * from #{table}")
      end
    end
  end
end
