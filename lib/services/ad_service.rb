# frozen_string_literal: true

require 'net/http'

module AdService
  class << self
    def get_remote_ads
      uri = URI('https://mockbin.org/bin/fcb30500-7b98-476f-810d-463a0b8fc3df')
      Net::HTTP.get(uri)
    end
  end
end
