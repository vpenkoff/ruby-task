# frozen_string_literal: true

module CampaignService
  class << self
    def process_request(request)
      return get_all_campaigns if request.env['REQUEST_METHOD'] == 'GET'

      return create_campaign(request.body) if request.env['REQUEST_METHOD'] == 'POST' && request.body

      ['400', { 'Content-Type' => 'text/html' }, ['Bad Request']]
    end

    def create_campaign(request_body)
      payload = JSON.parse(request_body.read)
      res = Resources::Campaign.create(payload)
      ['204', { 'Content-Type' => 'application/json' }, [res.to_json]]
    end

    def get_all_campaigns
      res = Resources::Campaign.get_all
      ['200', { 'Content-Type' => 'application/json' }, [res.to_json]]
    end
  end
end
