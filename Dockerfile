FROM ruby:2.6-stretch

WORKDIR /app

COPY . /app

RUN gem install bundler

RUN bundle install

EXPOSE 9292

CMD bundle exec rackup --host 0.0.0.0
