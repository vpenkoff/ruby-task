### Task background

We publish our jobs to different marketing sources. To keep track of where the particular job is published, we create
`Campaign` entity in database. `Campaigns` are periodically synchronized with 3rd party _Ad Service_.

`Campaign` properties:

- `id`
- `job_id`
- `status`: one of [active, paused, deleted]
- `external_reference`: corresponds to Ad’s ‘reference’
- `ad_description`: text description of an Ad

Due to various types of failures (_Ad Service_ inavailability, errors in campaign details etc.)
local `Campaigns` can fall out of sync with _Ad Service_.
So we need a way to detect discrepancies between local and remote state.

### TODOs
1. Develop a [Service](https://medium.com/selleo/essential-rubyonrails-patterns-part-1-service-objects-1af9f9573ca1)(as in _Service Object_ pattern),
which would get campaigns from external JSON API([example link](https://mockbin.org/bin/fcb30500-7b98-476f-810d-463a0b8fc3df)) and detect discrepancies between local and remote state.
### Service output format
You're free to choose the output format which makes sense to you, we suggest the following:
```
[
  {
    "remote_reference": "1",
    "discrepancies": [
      "status": {
        "remote": "disabled",
        "local": "active"
      },
      "description": {
        "remote": "Rails Engineer",
        "local": "Ruby on Rails Developer"
      }
    ]
  }
]
```

### Solution
Here's an example solution to for the task above.

## prerequisites:
1. Ruby 2.6
2. Docker (optional)

## In order to run (locally):
1. rvm use 2.6
2. gem install bundler
3. bundle install
4. bundle exec rackup
5. curl -XGET "http://localhost:9292/campaigns"

## In order to run in docker:
1. docker build --tag=sometag .
2. docker run -p 9292:9292 sometag
3. curl -XGET "http://localhost:9292/campaigns"


