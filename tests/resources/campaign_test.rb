# frozen_string_literal: true

require 'minitest/autorun'
require 'json'
require 'sqlite3'

require_relative '../../lib/services/ad_service'
require_relative '../../lib/services/db_service'
require_relative '../../lib/resources/campaign'

class TestCampaign < MiniTest::Test
  def test_get_all_success
    expected_result_get_remote_ads = '{ "ads": [ { "reference": "2", "status": "enabled", "description": "Description for campaign 11" } ] }'

    expected_result_db_campaigns = [[2, 2, 'paused', 2, 'test']]

    AdService.stub :get_remote_ads, expected_result_get_remote_ads do
      DBService.stub :select, expected_result_db_campaigns do
        result = Resources::Campaign.get_all
        assert_instance_of Array, result
        assert_includes result[0], :remote_reference
        assert_equal result[0][:remote_reference], '2'
        assert_includes result[0], :discrepancies
      end
    end
  end
end
