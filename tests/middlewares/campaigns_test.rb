# frozen_string_literal: true

require 'minitest/autorun'
require 'rack'

require_relative '../../lib/middlewares/campaigns'
require_relative '../../lib/services/campaign_service'

class TestCampaignsMiddleware < Minitest::Test
  def setup
    @app = proc {}
  end

  def test_response_empty_response
    env = Rack::Request.new(Rack::MockRequest.env_for('http://example.com:8080/'))
    status, _, body = Rack::Campaign.new(@app).call(env)
    assert_equal status, 200
    assert_equal body, []
  end

  def test_response
    expected_response_body = [
      {
        "discrepancies": [
          {
            "status": {
              "local": 'active',
              "remote": 'enabled'
            }
          },
          {
            "description": {
              "local": 'test',
              "remote": 'Description for campaign 11'
            }
          }
        ],
        "remote_reference": '1'
      }
    ]
    expected_response = ['200',
                         { 'Content-Type' => 'application/json' },
                         expected_response_body]

    env = Rack::MockRequest.env_for('http://example.com:8080/campaigns',
                                    method: 'get')

    CampaignService.stub :process_request, expected_response do
      status, _, body = Rack::Campaign.new(@app).call(env)
      assert_equal status, '200'
      assert_instance_of Array, body
      assert_includes body[0], :discrepancies
    end
  end

  def test_response_bad_request
    expected_response = ['400', { 'Content-Type' => 'application/json' }, ['Bad Request']]
    env = Rack::MockRequest.env_for('http://example.com:8080/campaigns',
                                    method: 'post')

    CampaignService.stub :process_request, expected_response do
      status, = Rack::Campaign.new(@app).call(env)
      assert_equal status, '400'
    end
  end
end
