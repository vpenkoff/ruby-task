# frozen_string_literal: true

require 'minitest/autorun'
require_relative '../../lib/services/ad_service'

class TestAdService < Minitest::Test
  def test_get_remote_ads_success
    expected_result = { "ads": [{ "reference": '1', "status": 'enabled', "description": 'Description for campaign 11' }] }
    AdService.stub :get_remote_ads, expected_result do
      response = AdService.get_remote_ads

      assert_includes response, :ads
      assert_instance_of Array, response[:ads]
    end
  end
end
