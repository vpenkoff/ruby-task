# frozen_string_literal: true

require 'sqlite3'

begin
  db = SQLite3::Database.new 'test.db'

  db.execute 'DROP TABLE IF EXISTS campaigns'

  rows = db.execute <<-SQL
  create table campaigns (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    job_id INT,
    status VARCHAR(30),
    external_reference INT,
    ad_description TEXT
  );
  SQL

  db.execute "INSERT INTO campaigns (job_id, status, external_reference, ad_description) VALUES (1, 'active', 1, 'test')"
  db.execute "INSERT INTO campaigns (job_id, status, external_reference, ad_description) VALUES (2, 'paused', 2, 'test')"
  db.execute "INSERT INTO campaigns (job_id, status, external_reference, ad_description) VALUES (3, 'deleted', 3, 'test')"

  db.commit
rescue SQLite3::Exception => e
  puts 'Exception occurred'
  puts e
ensure
  db&.close
end
