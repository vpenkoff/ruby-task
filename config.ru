# frozen_string_literal: true

require 'rubygems'
require 'bundler/setup'
require 'sqlite3'

require_relative 'lib/services/db_service'
require_relative 'lib/services/campaign_service'
require_relative 'lib/services/ad_service'
require_relative 'lib/middlewares/campaigns'
require_relative 'lib/resources/campaign'

app = Rack::Builder.new do
  map '/' do
    use Rack::Campaign
  end

  run ->(_env) { ['200', { 'Content-Type' => 'text/html' }, []] }
end

run app
